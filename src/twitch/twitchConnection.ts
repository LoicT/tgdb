import { TwitchApi } from 'node-twitch'
import { type Stream } from 'node-twitch/dist/types/objects'

export class TwitchConnection {
  public static readonly REMOVE_FROM_HISTORY_DELAY = 60 * 60 * 1000
  public static readonly MINIMUM_DELAY = 10 * 60 * 1000
  public currentStreams: Stream[] = []
  public newStreams: Stream[] = []
  private readonly twitchAPI: TwitchApi
  private lastUpdates: LastUpdate[] = []

  public constructor(
    private readonly client_id: string,
    private readonly client_secret: string,
    private readonly game_id: number) {
    this.twitchAPI = new TwitchApi({
      client_id, client_secret
    })
  }

  public async updateStreams(): Promise<void> {
    console.log('Update streams :')
    console.log(`=========== Current streams (${this.currentStreams.length}) ===========`)
    console.log(this.currentStreams.map(s => s.user_login))
    var streams: Stream[] = [];
    var done = false;
    while (!done) {
      try {
        streams = (await (this.twitchAPI.getStreams({ game_id: this.game_id })))
          .data.filter(stream => stream.game_id === this.game_id.toString())
        done = true;
      } catch (e) {
        console.log("Error while getting streams")
        console.log(e)
        console.log("Retry in 10 seconds")
        await new Promise(resolve => setTimeout(resolve, 10000))
      }
    }

    // Filter new streams from current streams
    this.newStreams = streams.filter(stream => {
      const update = this.lastUpdates.find(s => s.user_id == stream.user_id)
      if ((update != null) && update.lastUpdate < TwitchConnection.MINIMUM_DELAY) {
        return false
      }
      return !this.currentStreams.map(x => x.user_id).includes(stream.user_id)
    })
    console.log(`=========== New streams (${this.newStreams.length}) ===========`)
    console.log(this.newStreams.map(s => s.user_login))

    this.currentStreams = streams
    this.currentStreams.forEach(x => {
      this.updateStream(x)
    })

    this.lastUpdates = this.lastUpdates.filter(x => Date.now() - x.lastUpdate > TwitchConnection.REMOVE_FROM_HISTORY_DELAY)
  }

  public async getGame(name: string) {
    console.log(await this.twitchAPI.getGames(name))
  }

  private updateStream(stream: Stream) {
    const s = this.lastUpdates.find(x => x.user_id == stream.user_id)
    if (s != null) {
      s.lastUpdate = Date.now()
    } else {
      this.lastUpdates.push({ user_id: stream.user_id, lastUpdate: Date.now() })
    }
  }
}

interface LastUpdate {
  user_id: string
  lastUpdate: number
}
