import { TwitchConnection } from './twitch/twitchConnection'
import { DiscordBot } from './discord/discordBot'

const twitchConfigs = require('../config/twitch_config.json')
const connection = new TwitchConnection(
  twitchConfigs.client_id,
  twitchConfigs.client_secret,
  twitchConfigs.game_id)
const bot = new DiscordBot(connection)

async function main(): Promise<void> {
  console.log('Discord Bot:')
  await bot.init()
  while (true) {
    console.log("Heart beat : " + new Date().toLocaleString())
    await new Promise(resolve => setTimeout(resolve, 60000))
  }
}

main()
  .then()
