import { type CommandInteraction, type Interaction } from 'discord.js'
import { type DiscordBot } from '../discordBot'
import { Commands } from '../commands/commands'

export default (bot: DiscordBot): void => {
  bot.client.on('interactionCreate', async (interaction: Interaction) => {
    if (interaction.isCommand() || interaction.isContextMenuCommand()) {
      await handleSlashCommand(bot, interaction as CommandInteraction)
    }
  })
}

const handleSlashCommand = async (bot: DiscordBot, interaction: CommandInteraction): Promise<void> => {
  const slashCommand = Commands.find(c => c.command.name === interaction.commandName)
  if (slashCommand == null) {
    interaction.followUp({ content: 'An error has occurred' })
    return
  }

  await interaction.deferReply()

  slashCommand.run(bot, interaction)
}
