import { type Client } from 'discord.js'
import { Commands } from '../commands/commands'

export default (client: Client): void => {
  client.on('ready', async () => {
    if ((client.user == null) || (client.application == null)) {
      return
    }

    Commands.forEach(c => {
      console.log(`Command ${c.command.name} added`)
    })

    await client.application.commands.set(Commands.map(x => x.command))

    console.log(`${client.user.username} is online`)
  })
}
