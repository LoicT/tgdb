// Require the necessary discord.js classes
import { Client, type TextChannel, type Message } from 'discord.js'
import ready from './listeners/ready'
import interactionCreate from './listeners/interactionCreate'
import { type TwitchConnection } from '../twitch/twitchConnection'
import { Stream } from 'node-twitch/dist/types/objects'
const { token, default_refresh_delay } = require('../../config/discord_config.json')

export class DiscordBot {
  public client: Client
  public channel?: TextChannel
  public started: boolean = false
  public refreshTime: number;
  private streamsMessage: StreamMessage[] = [];

  private readonly escapeCharacters = ['*', '`', '>', '_']

  public constructor(public twitch: TwitchConnection) {
    this.client = new Client({ intents: [] })
    this.refreshTime = default_refresh_delay
  }

  public async init(): Promise<void> {
    console.log('Init Discord Bot:')
    // Log when the bot is ready
    ready(this.client)
    // Add interations
    interactionCreate(this)

    // Log in to Discord with your client's token
    await this.client.login(token)
  }

  public async start(): Promise<void> {
    while (this.started) {
      await this.twitch.updateStreams()
      // Notify new streams
      for (const stream of this.twitch.newStreams) {
        console.log(`New stream by ${stream.user_login} sending message...`);
        try {
          this.streamsMessage.push(
            {
              stream,
              message: (await this.publishMessageInChannel(this.messageBuilder(stream, true)))
            }
          )
          console.log(`Message successfully sent`);
        } catch (e) {
          console.error(`Error whil sending message : `);
          console.error(e);
        }

      }
      // Change message when stream goes offline
      for (const message of this.streamsMessage) {
        if (!this.twitch.currentStreams.find(x => x.user_id == message.stream.user_id)) {
          console.log(`Stream of ${message.stream.user_login} ended modifying message...`)
          await message.message.edit(this.messageBuilder(message.stream, false));
          console.log(`Message modified successfully`);
          this.streamsMessage = this.streamsMessage.filter(m => !(m.stream.user_id === message.stream.user_id));
        }
      }
      // Wait before next update
      await new Promise(resolve => setTimeout(resolve, this.refreshTime))
    }
  }

  public async publishMessageInChannel(message: string): Promise<Message<true>> {
    if (this.channel === undefined) {
      throw new Error('Target channel not initialized')
    }
    return await this.channel.send(message)
  }

  private escape(message: string, char: string) {
    return message.replace(char, `\\${char}`)
  }

  private messageBuilder(stream: Stream, online: boolean): string {
    var message = '';
    if (online) {
      message = `🟢 ${stream.user_name} is streaming ${stream.game_name} at https://www.twitch.tv/${stream.user_login}`
    } else {
      message = `🔴 ${stream.user_name} was streaming ${stream.game_name} at https://www.twitch.tv/${stream.user_login}`
    }
    this.escapeCharacters.forEach(char => message = this.escape(message, char))
    return message;
  }
}

interface StreamMessage {
  stream: Stream;
  message: Message<true>
}
