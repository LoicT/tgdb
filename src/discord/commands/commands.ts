import { type CommandInteraction, SlashCommandBuilder, type TextChannel } from 'discord.js'
import { type DiscordBot } from '../discordBot'
import { type Command, createSimpleCommand } from './command'

const Init: Command = {
  command: new SlashCommandBuilder()
    .setName('set_channel')
    .setDescription('Set the channel of the bot')
    .addChannelOption(option =>
      option
        .setName('channel')
        .setDescription('The channel to send the messages in')
        .setRequired(true)),
  run: async (client: DiscordBot, interaction: CommandInteraction) => {
    client.channel = interaction.options.get('channel')?.channel as TextChannel
    const content = `Bot channel set as ${client.channel?.name}`
    await interaction.followUp({
      ephemeral: true,
      content
    })
  }
}

const SetRefreshTime: Command = {
  command: new SlashCommandBuilder()
    .setName('refresh_time')
    .setDescription('Set the refresh time of the bot')
    .addNumberOption(option =>
      option
        .setName('refresh_time')
        .setDescription('The refresh time of the bot in ms')
        .setRequired(true)),
  run: async (client: DiscordBot, interaction: CommandInteraction) => {
    if (interaction.options.get('refresh_time') == null) {
      throw new Error('Value is not a number')
    }
    client.refreshTime = interaction.options.get('refresh_time')!.value as number

    const content = `Bot refresh time set as ${client.refreshTime}`
    await interaction.followUp({
      ephemeral: true,
      content
    })
  }
}

const Start: Command = createSimpleCommand('start', 'Start the bot',
  async (client: DiscordBot, interaction: CommandInteraction) => {
    client.started = true
    await interaction.followUp({
      ephemeral: true,
      content: 'Bot started'
    })
    client.start()
  })

const Stop: Command = createSimpleCommand('stop', 'Stop the bot',
  async (client: DiscordBot, interaction: CommandInteraction) => {
    client.started = false
    await interaction.followUp({
      ephemeral: true,
      content: 'Bot stopped'
    })
  })

export const Commands: Command[] = [Init, Start, Stop, SetRefreshTime]
