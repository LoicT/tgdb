import { SlashCommandBuilder, type CommandInteraction } from 'discord.js'
import { type DiscordBot } from '../discordBot'

export interface Command {
  command: any
  run: (client: DiscordBot, interaction: CommandInteraction) => Promise<void>
}

export function createSimpleCommand(name: string, description: string, run: (client: DiscordBot, interaction: CommandInteraction) => Promise<void>): Command {
  return {
    command: new SlashCommandBuilder()
      .setName(name)
      .setDescription(description),
    run
  }
}